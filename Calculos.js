function programa() {
  senos();
  cosenos();
  tangentes();
}
let a, b, i, aux;
function senos() {
  console.log("Senos");
  a = document.getElementById("n1").value;
  b = document.getElementById("n2").value;
  if (a < b) {
    aux = a;
    for (i = a; i <= b; i++) {
      console.log(aux);
      console.log(Math.sin(aux));
      aux++;
    }
  } else {
    aux = b;
    for (i = b; i <= a; i++) {
      console.log(aux);
      console.log(Math.sin(aux));
      aux++;
    }
  }
}
function cosenos() {
  console.log("Cosenos");
  a = document.getElementById("n1").value;
  b = document.getElementById("n2").value;
  if (a < b) {
    aux = a;
    for (i = a; i <= b; i++) {
      console.log(aux);
      console.log(Math.cos(aux));
      aux++;
    }
  } else {
    aux = b;
    for (i = b; i <= a; i++) {
      console.log(aux);
      console.log(Math.cos(aux));
      aux++;
    }
  }
}
function tangentes() {
  console.log("Tangentes");
  a = document.getElementById("n1").value;
  b = document.getElementById("n2").value;
  if (a < b) {
    aux = a;
    for (i = a; i <= b; i++) {
      console.log(aux);
      console.log(Math.tan(aux));
      aux++;
    }
  } else {
    aux = b;
    for (i = b; i <= a; i++) {
      console.log(aux);
      console.log(Math.tan(aux));
      aux++;
    }
  }
}
